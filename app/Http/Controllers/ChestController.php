<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ChestController extends Controller
{
    public function index($type)
    {
        $file = null;
        $opened = false;

        return view('Chests.index', compact('type', 'file', 'opened'));
    }

    public function create($type)
    {
        return view('Chests.create', compact('type'));
    }

    public function store()
    {
        //image name:
        //type_name_underscores.png
    }

    public function edit()
    {
        return view('Chests.edit', compact('type'));
    }

    public function update()
    {
        //image name:
        //type_name_underscores.png
    }

    public function destroy()
    {
        //delete images
    }

    /**
     * Aditional methods.
     *
     */
    public function homePage() {
        $chests = json_decode(file_get_contents(base_path('public/storage/database/chests.json')));

        return view('home', compact('chests'));
    }

    public function detail($type)
    {
        return view('Chests.detail', compact('type'));
    }

    public function open($type)
    {
        $files = Storage::files('/public/images/' . $type . '/items/');

        $sort = rand(0, (count($files) - 1));

        $file = $files[$sort];

        $opened = true;

        return view('Chests.index', compact('type', 'file', 'opened'));
    }
}
