<?php

return [
    'welcome' => 'Welcome!',
    'openChest' => 'Click the button to open a chest',
    'actions' => [
        'open' => 'Open',
        'reopen' => 'Reopen',
        'return' => 'Return to Home',
        'seeDetails' => 'See details',
    ],
];
