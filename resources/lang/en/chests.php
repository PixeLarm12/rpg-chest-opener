<?php

return [
    'alchemist' => [
        'title' => "Alchemist's Chest",
        'description' => "",
    ],
    'armorer' => [
        'title' => "Armorer's Chest",
        'description' => "",
    ],
    'blacksmith' => [
        'title' => "Blacksmith's Chest",
        'description' => "",
    ],
    'legendary' => [
        'title' => 'Legendary Chest',
        'description' => "",
    ],
    'tools' => [
        'title' => 'Tool Chest',
        'description' => "",
    ],
    'wooden' => [
        'title' => 'Wooden Chest',
        'description' => "",
    ],
];
