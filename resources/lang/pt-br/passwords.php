<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sua senha foi restaurada!',
    'sent' => 'Enviamos para o seu e-mail o link para restaurar a senha!',
    'throttled' => 'Por favor, espere para tentar novamente.',
    'token' => 'Esse token de senha é inválido.',
    'user' => "Não conseguimos encontrar um usuário com esse endereço de e-mail.",

];
