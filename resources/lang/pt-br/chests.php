<?php

return [
    'alchemist' => [
      'title' => 'Baú do Alquimista',
      'description' => '',
    ],
    'armorer' => [
        'title' => 'Baú de Armadura',
        'description' => '',
      ],
    'blacksmith' => [
        'title' => 'Baú de Armamentos',
        'description' => '',
      ],
    'legendary' => [
        'title' => 'Baú Lendário',
        'description' => '',
      ],
    'tools' => [
        'title' => 'Baú de Ferramentas',
        'description' => '',
      ],
    'wooden' => [
        'title' => 'Baú de Madeira',
        'description' => '',
      ],
];
