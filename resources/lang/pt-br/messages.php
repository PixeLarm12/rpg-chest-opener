<?php

return [
    'welcome' => 'Bem-vindo!',
    'openChest' => 'Clique no botão para abrir o baú',
    'actions' => [
        'open' => 'Abrir',
        'reopen' => 'Abrir novamente',
        'return' => 'Voltar para a Página Inicial',
        'seeDetails' => 'Ver detalhes',
    ],
];
