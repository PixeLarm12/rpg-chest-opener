@extends('../Templates/main')

@section('title', Str::ucfirst($type) . ' - ' . trans('pages.details'))

@section('content')

    <h1>{{ Str::ucfirst($type) }} - @lang('pages.details')</h1>

@endsection
