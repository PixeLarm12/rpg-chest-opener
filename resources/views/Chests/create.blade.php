@extends('../Templates/main')

@section('title', Str::ucfirst($type) . ' - ' . trans('pages.create'))

@section('content')

    <h1>{{ Str::ucfirst($type) }} - @lang('pages.create')</h1>

@endsection
