@extends('../Templates/main')

@section('title', Str::ucfirst($type) . ' - ' . trans('pages.edit'))

@section('content')

    <h1>{{ Str::ucfirst($type) }} - @lang('pages.edit')</h1>

@endsection
