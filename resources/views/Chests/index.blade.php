@extends('../Templates/main')

@section('title', Str::ucfirst($type) . ' - ' . trans('pages.home'))

@section('content')

<div class="d-flex justify-content-center">
    <div class="row">
        <div class="col-5">
            <h1> @lang('pages.home')</h1>
            <h3> <i>{{ Str::ucfirst($type) }}</i> </h3>

            <div class="row mt-5">
                <div class="col-12">
                    @if (! $opened)
                        <a href="/chests/{{$type}}/open" class="btn btn-main-light w-100">@lang('messages.actions.open')</a>
                        <a href="/chests/" class="btn btn-main-light w-100 mt-2">@lang('messages.actions.return')</a>
                    @else
                        <a href="/chests/{{$type}}" class="btn btn-main-light w-100">@lang('messages.actions.reopen')</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-7">
            <div class="row">
                <div class="col-12">
                    <div class="card text-white border border-main-border mt-5" style="width: 600px">
                        <img
                        class="card-img-top"
                        width="600px"
                        height="auto"
                        src="{{ $file == null ? Storage::url('public/images/locked_chest.png') : Storage::url($file) }}">

                        <div class="card-body bg-main-light text-wrap">
                            <h4 class="card-title">@lang('chests.' . $type . '.title')</h4>
                            <p class="card-text">
                                @if($file == null)
                                @lang('chests.' . $type . '.description')
                                @else
                                {{ basename($file) }}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
