@extends('Templates.main')

@section('title', 'Home Page')

@section('content')

<div class="d-flex justify-content-around">

    <h1>@lang("pages.home")</h1>

    <div class="row">
        @foreach($chests as $key => $chest)
            <div class="col-4">
                <div class="card text-white border border-main-border mt-5" style="width: 18rem;">
                    <img class="card-img-top" src="{{ Storage::url('public/images/' . $chest->url) }}">
                    <div class="card-body bg-main-light text-wrap">
                        <h4 class="card-title">@lang('chests.' . $chest->type . '.title')</h4>
                        <p class="card-text">@lang('chests.' . $chest->type . '.description')</p>
                        <a href='/chests/{{ $chest->type }}' class="btn btn-main-dark mt-2 w-100">@lang('messages.actions.open') - @lang('chests.' . $chest->type . '.title')</a>
                        <a href='/chests/{{ $chest->type }}/details' class="btn btn-main-dark mt-2 w-100">@lang('messages.actions.seeDetails')</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
