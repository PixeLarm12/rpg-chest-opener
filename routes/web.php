<?php

use App\Http\Controllers\ChestController;

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\ChestController@homePage');
Route::get('/chests', 'App\Http\Controllers\ChestController@homePage');

Route::get('/chests/{type}/details', 'App\Http\Controllers\ChestController@detail');
Route::get('/chests/{type}/open', 'App\Http\Controllers\ChestController@open');

Route::resource('/chests/{type}', App\Http\Controllers\ChestController::class);

